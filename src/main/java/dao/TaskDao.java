package dao;

import model.Task;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class TaskDao {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public Task insertTask(Task task) {
        if (task.getId() == null) {
            em.persist(task);
        } else
            em.merge(task);
        return task;
    }

    @Transactional
    public void deleteAllTasks() {
        em.createQuery("delete from Task").executeUpdate();
    }

    @Transactional
    public void deleteTaskById(Long id) {
        Query query = em.createQuery(
                "delete from Task as t where t.id = :id");
        query.setParameter("id", id);
        query.executeUpdate();
    }

    public List<Task> getAllTasks() {
        return em.createQuery("select t from Task t", Task.class).getResultList();
    }

    public Task getTaskById(Long id) {
        TypedQuery<Task> query = em.createQuery(
                "select t from Task t where t.id = :id",
                Task.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }
}
