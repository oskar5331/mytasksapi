package controller;

import dao.TaskDao;
import model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TaskController {

    @Autowired
    private TaskDao dao;

    @PostMapping("tasks")
    public Task newTask(@RequestBody Task task) {
        return dao.insertTask(task);
    }

    @PostMapping("tasks/{id}")
    public Task taskUpdate(@RequestBody Task task, @PathVariable("id") Long id) {
        task.setId(id);
        return dao.insertTask(task);
    }

    @GetMapping("tasks/{id}")
    public Task getTaskById(@PathVariable("id") Long id) {
        return dao.getTaskById(id);
    }

    @GetMapping("tasks")
    public List<Task> getAllTasks() {
        return dao.getAllTasks();
    }

    @DeleteMapping("tasks/{id}")
    public void deleteTaskById(@PathVariable("id") Long id) {
        dao.deleteTaskById(id);
    }

    @DeleteMapping("tasks")
    public void deleteTasks() {
        dao.deleteAllTasks();
    }

    @PutMapping("tasks/{id}")
    public void taskDone(@PathVariable("id") Long id) {
        Task task = dao.getTaskById(id);
        task.taskDone();
        dao.insertTask(task);
    }

}
